const clientId = 'ee76a58911f2430b88d70714bb3c79d2';
const clientSecret = 'cbdfef875f58479880eaea12fe134826';


async function getProfile(accessToken) {
  const response = await fetch('https://api.spotify.com/v1/me', {
    headers: {
      Authorization: 'Bearer ' + accessToken
    }
  });

  const data = await response.json(); 
  return data;
}
async function getToken(){
const accessToken_r = await fetch('https://accounts.spotify.com/api/token', {
            method: 'POST',
            headers: {
                'Content-Type' : 'application/x-www-form-urlencoded', 
                'Authorization' : 'Basic ' + btoa( clientId + ':' + clientSecret)
            },
            body: 'grant_type=client_credentials'
        });
const accessToken = (await accessToken_r.json())['access_token'];
return accessToken;
}
getToken().then(a => getProfile(a)).then(a => console.log(a));

